package main

import (
	"fmt"
	"strings"
)

func RomanReturn(n int) string {

	conversions := []struct {
		number int
		roman  string
	}{
		{1000, "M"},
		{900, "CM"},
		{500, "D"},
		{400, "CD"},
		{100, "C"},
		{90, "XC"},
		{50, "L"},
		{40, "XL"},
		{10, "X"},
		{9, "IX"},
		{5, "V"},
		{4, "IV"},
		{1, "I"},
	}
	var roman []string
	// var roman strings.Builder
	for _, convert := range conversions {
		for n >= convert.number {
			roman = append(roman, convert.roman)
			n = n - convert.number
		}
	}
	return strings.Join(roman, "")
}

func main() {
	fmt.Println("enter number between 1 to 4000")
	var n int
	fmt.Scan(&n)

	// arr := strings.SplitAfter(fmt.Sprint(n), "")
	roman := RomanReturn(n)
	fmt.Println("roman equivalent is", roman)
}
