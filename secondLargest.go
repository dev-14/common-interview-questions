package main

import "fmt"

func main() {
	arr := []int{12, 35, 1, 10, 34, 1}
	l1 := arr[0]
	l2 := 0

	for i := 1; i <= 5; i++ {
		if l1 < arr[i] {
			l2 = l1
			l1 = arr[i]
		} else if l2 < arr[i] {
			l2 = arr[i]
		}
	}
	fmt.Println(l1, "is the largest inn the array")
	fmt.Println(l2, "is the second largest inn the array")
}
