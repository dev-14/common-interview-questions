package main

import "fmt"

func prime(n int) []int {
	var arr []int
	count := 0
	for i := 2; i <= n; i++ {
		for j := 1; j <= i; j++ {
			if i%j == 0 {
				count++
			}
			if count > 2 {
				break
			} else if i == j {
				arr = append(arr, i)
			}
		}
		count = 0
	}
	return arr
}

func main() {

	fmt.Println("enter number between 1 to 100")
	var n int
	fmt.Scanf("%d", &n)
	arr := prime(n)
	fmt.Println(arr, "are the prime nos")
}
