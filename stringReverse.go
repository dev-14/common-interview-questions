package main

import "fmt"

func main() {
	var str string
	fmt.Println("enter string")
	fmt.Scanln(&str)
	byte_str := []rune(str)
	for i, j := 0, len(byte_str)-1; i < j; i, j = i+1, j-1 {
		byte_str[i], byte_str[j] = byte_str[j], byte_str[i]
	}
	fmt.Println("output is", string(byte_str))
}
