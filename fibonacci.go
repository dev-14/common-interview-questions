package main

import "fmt"

func main() {
	var n int
	fmt.Println("enter number of terms")
	fmt.Scanf("%d", &n)
	var arr1 []int
	fibo(n-2, 0, 1, arr1)
}

func fibo(n int, a int, b int, arr []int) {
	if a == 0 {
		arr = append(arr, a)
		arr = append(arr, b)
	}

	if n > 0 {
		c := a + b
		a = b
		b = c
		arr = append(arr, c)
		fibo(n-1, a, b, arr)
		return
	}
	fmt.Println(arr)
}
