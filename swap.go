package main

import "fmt"

func main() {
	a := 10
	b := 20
	// var a string
	a, b = b, a
	fmt.Println("a is", a)
	fmt.Println("b is", b)
}
