package main

import (
	"fmt"
)

func repeat(arr []int) []int {
	var elems []int
	for i := 0; i < len(arr); i++ {
		for j := i + 1; j < len(arr); j++ {
			if arr[i] == arr[j] {
				elems = append(elems, arr[i])
				break
			}
		}
	}
	return elems
}

func main() {
	fmt.Println("enter number of elements")
	var n int
	fmt.Scan(&n)
	fmt.Println("enter elements")
	arr := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &arr[i])
	}
	elems := repeat(arr)
	fmt.Println("repeated elements are:", elems)
}
