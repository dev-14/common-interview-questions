package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println("enter a number")
	var n string
	fmt.Scan(&n)
	arr := strings.SplitAfter(n, "")
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}
	output := strings.Join(arr, "")
	if n == output {
		fmt.Println("Palindrome")
	} else {
		fmt.Println("not palindrome")
	}
	// for _, val := range arr {
	// 	fmt.Print(val)
	// }
}
