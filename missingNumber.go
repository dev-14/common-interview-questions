package main

import "fmt"

func main() {
	sum := 0

	input := []int{1, 2, 3, 4, 6, 7, 8}

	for i := 0; i < len(input); i++ {
		sum += input[i]
	}
	n := len(input)
	total := (n + 1) * (n + 2) / 2
	fmt.Println(total - sum)
}
