package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("enter number")
	var n float64
	fmt.Scan(&n)
	N := math.Sqrt(n)
	fmt.Printf("square root of %.0f is  %.2f", n, N)
}
