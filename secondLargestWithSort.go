package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("enter number of elements")
	var n int
	fmt.Scan(&n)
	arr := make([]int, n)
	fmt.Println("enter elements of array")
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &arr[i])
	}
	// fmt.Println(arr)
	sort.Ints(arr)
	fmt.Println("second largest element of array is", arr[n-2])
}
