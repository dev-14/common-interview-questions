package main

import "fmt"

func main() {
	fmt.Println("enter number of elements in list")
	var n int
	fmt.Scan(&n)
	arr := make([]int, n)
	fmt.Println("enter elements")
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &arr[i])
	}
	evenArray := even(arr)
	fmt.Println("even numbers are", evenArray)
}

func even(arr []int) []int {
	var even []int
	for i := 0; i < len(arr); i++ {
		if arr[i]%2 == 0 {
			even = append(even, arr[i])
		}
	}
	return even
}
